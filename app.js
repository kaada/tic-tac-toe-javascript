const PLAYER_TOKEN = 'X'
const COMPUTER_TOKEN = 'Y'

$(document).ready(function(){
  let grid = [
    [' ', ' ', ' '],
    [' ', ' ', ' '],
    [' ', ' ', ' ']
  ];

  function isGameOver(){
    for(var i = 0; i<3; i++){
      if(grid[i][0] !== ' ' &&
         grid[i][0] === grid[i][1] &&
         grid[i][0] === grid[i][2]){
           return grid[i][0];
         }
    }

    for(var j = 0; j<3;j++){
      if(grid[0][j] !== ' ' &&
         grid[0][j] === grid[1][j] &&
         grid[0][j] === grid[2][j]){
           return grid[0][j];
         }
    }

    if(grid[0][0] !== ' ' &&
       grid[0][0] === grid[1][1] &&
       grid[0][0] === grid[2][2]){
         return grid[0][0];
       }
    if(grid[0][2] !== ' ' &&
       grid[0][2] === grid[1][1] &&
       grid[0][2] === grid[2][0]){
         return grid[0][2];
       }

    for(var i = 0; i<3; i++){
      for(var j = 0; j<3; j++){
        if(grid[i][j] === ' '){
          return false;
        }
      }
    }

    return null;
  }

  function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  function moveAI(){
    while(true){
      var i = getRandomInt(0,2);
      var j = getRandomInt(0,2);

      if(grid[i][j] === ' '){
        return{
          i: i,
          j: j
        }
      }
    }
  }

  $('.col').click(function() {
    $(this).html(PLAYER_TOKEN);
    const i = $(this).data('i');
    const j = $(this).data('j');
    if(grid[i][j] !== ' '){
      return;
    }
    grid[i][j] = PLAYER_TOKEN;

    let gameState = isGameOver();

    if(gameState){
      alert('GAME OVER! ' + gameState + ' WON!');
      return;
    } else{
      const move = moveAI();
      grid[move.i][move.j] = COMPUTER_TOKEN;
      $('.col[data-i=' + move.i + '][data-j=' + move.j + ']').html(COMPUTER_TOKEN);
    }

    if(gameState){
      alert('GAME OVER! ' + gameState + ' WON!');
      return;
    }
  });

  $('#restart').click(function() {
    grid = [
      [' ', ' ', ' '],
      [' ', ' ', ' '],
      [' ', ' ', ' ']
    ];
    for(var i = 0; i<3; i++){
      for(var j = 0; j<3; j++){
        $('.col[data-i=' + i + '][data-j=' + j + ']').html('-');
      }
    }

  });

});
